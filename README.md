#REA

##Recursos Educativos Abiertos
Los recursos educativos digitales son la herramienta esencial para cualquier docente.

Desde 2008 Ceibal crea recursos educativos digitales. Los mismos cumplen los más exigentes niveles de calidad: educativos, interactivos, motivadores, eficaces.

Están diseñados para el aprendizaje autónomo por lo que los niños, niñas y jóvenes pueden acceder de manera individual o colectiva de acuerdo a objetivos educativos o para satisfacer inquietudes personales.

Distintos diseños permiten brindar diversas oportunidades de aprendizaje considerando secuencias ordenadas en forma dependiente, siguiendo una estructura conceptual o configurando espacios de deducción o retos de investigación.

Se potencia así el aprendizaje personal y/o colaborativo en ambientes virtuales y se simplifica la labor del docente.

## Más acerca de REA

[1] https://rea.ceibal.edu.uy/

[2] https://www.youtube.com/watch?time_continue=5&v=DwY4N2dkIwM

[3] http://blogs.ceibal.edu.uy/formacion/recursos-educativos-abiertos/

## Tecnología
El repositorio de Recursos Educativos Abiertos **(REA)** está desarrollado en OctoberCMS, el cual cumple con diversas politicas de seguridad que hacen del mismo seguro y versatil en cuanto a lo que se puede realizar en el.

## Instalación
1. Clona el repositorio
   ``` git clone git@bitbucket.org:ceibal/rea-ceibal.git ```
2. Crea una base de datos
    Se recomienda usar mysql para la misma
3. Generar el archivo .env copiando el .env-example
    ``` $ cp .env-example .env```
4. Asignar las credenciales de la base de datos al archivo .env
5. Ejecutar ```**composer install** ```
6. Ejecutar ```**php artisan october:up**``` para correr las migraciones
7. Podras validar el funcionamiento del repositorio sin crear un virtualhost ejecutando el siguiente comando ```**php artisan serve** ```


     

