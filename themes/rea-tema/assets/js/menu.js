jQuery(document).ready(function($){
	//if you change this breakpoint in the style.css file (or _layout.scss if you use SASS), don't forget to update this value as well
	var MqL = 1170;
	//move nav element position according to window width
	moveNavigation();
	$(window).on('resize', function(){
		(!window.requestAnimationFrame) ? setTimeout(moveNavigation, 300) : window.requestAnimationFrame(moveNavigation);
	});

	//mobile - open lateral menu clicking on the menu icon
	$('.cd-nav-trigger').on('click', function(event){
		event.preventDefault();
		if( $('.cd-main-content').hasClass('nav-is-visible') ) {
			closeNav();
			$('.cd-overlay').removeClass('is-visible');
		} else {
			$('.recursos-home.ficha').css("z-index",0);
			$(this).addClass('nav-is-visible');
			$('.cd-primary-nav').addClass('nav-is-visible');
			$('.cd-main-header').addClass('nav-is-visible');
			$('.cd-main-content').addClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
				$('body').addClass('overflow-hidden');
			});
			toggleSearch('close');
			$('.cd-overlay').addClass('is-visible');
		}
	});

	//open search form
	$('.cd-search-trigger').on('click', function(event){
		event.preventDefault();
		toggleSearch();
		closeNav();
	});

	//close lateral menu on mobile
	$('.cd-overlay').on('swiperight', function(){
		if($('.cd-primary-nav').hasClass('nav-is-visible')) {
			closeNav();
			$('.cd-overlay').removeClass('is-visible');
		}
	});
	$('.nav-on-left .cd-overlay').on('swipeleft', function(){
		if($('.cd-primary-nav').hasClass('nav-is-visible')) {
			closeNav();
			$('.cd-overlay').removeClass('is-visible');
		}
	});
	$('.cd-overlay').on('click', function(){
		closeNav();
		toggleSearch('close')
		$('.cd-overlay').removeClass('is-visible');
	});


	//prevent default clicking on direct children of .cd-primary-nav
	$('.cd-primary-nav').children('.has-children').children('a').on('click', function(event){
		event.preventDefault();
	});
	//open submenu
	$('.has-children').children('a').on('click', function(event){
		if( !checkWindowWidth() ) event.preventDefault();
		var selected = $(this);
		if( selected.next('ul').hasClass('is-hidden') ) {
			//desktop version only
			selected.addClass('selected').next('ul').removeClass('is-hidden').end().parent('.has-children').parent('ul').addClass('moves-out');
			selected.parent('.has-children').siblings('.has-children').children('ul').addClass('is-hidden').end().children('a').removeClass('selected');
			$('.cd-overlay').addClass('is-visible');
		} else {
			selected.removeClass('selected').next('ul').addClass('is-hidden').end().parent('.has-children').parent('ul').removeClass('moves-out');
			$('.cd-overlay').removeClass('is-visible');
		}
		toggleSearch('close');
	});

	//submenu items - go back link
	$('.go-back').on('click', function(){
		var isCerrar = $(this).hasClass("cerrar");
		if(isCerrar)
		{
			$("a.cd-nav-trigger.menu-cat").trigger("click");
		}else
		{
			$(this).parent('ul').addClass('is-hidden').parent('.has-children').parent('ul').removeClass('moves-out');
		}

	});

	$(".cd-search-trigger").on('click',function(){
		$('.ss-search-form__input input').focus();
	});

	$("#destacados").on("click",function()
	{
		selectMenu($(this));
		getRecursosAjax("backend/ceibal/rea/api/destacados");
	});

	$("#mas-vistos").on("click",function()
	{
		selectMenu($(this));
		getRecursosAjax("backend/ceibal/rea/api/masvistos");
	});

	$("#ultimos-subidos").on("click",function()
	{
		selectMenu($(this));
		getRecursosAjax("backend/ceibal/rea/api/ultimos");
	});

	$(".filtro-resp-cat").change(function()
	{

		var filtro = parseInt($(this).val());
		window.location.href = "?c=" + filtro;
	});

	$(".filtro-resp").change(function()
	{
		var filtro = parseInt($(this).val());
		switch (filtro)
		{
			case 1:
				selectMenu($('#destacados'));
				getRecursosAjax("backend/ceibal/rea/api/destacados");
			break;
			case 2:
				selectMenu($('#mas-vistos'));
				getRecursosAjax("backend/ceibal/rea/api/masvistos");
			break;
			case 3:
				selectMenu($('#ultimos-subidos'));
				getRecursosAjax("backend/ceibal/rea/api/ultimos");
			break;
		}

	});





	function getRecursosAjax(url)
	{
		$.ajax({
          url: url,
          beforeSend: function () {
                  //$("#cards-recursos").html("Procesando, espere por favor...");
									$("#cards-recursos").html("");
									$(".spinner").removeClass("hidden");
          },
          success:  function (response)
					{
							$(".spinner").addClass("hidden");
							var resp = response.recursosDestacados;

							resp.forEach(function(data)
							{
								$("#cards-recursos").append(renderRecurso(data));

								limitarTextos();

							});
          }
      });
	}

	function selectMenu(menu)
	{
		$("#destacados").removeClass("active");
		$("#mas-vistos").removeClass("active");
		$("#ultimos-subidos").removeClass("active");

		menu.addClass("active");

	}

	function renderRecurso(data)
	{
		var recurso = '<div class="col-sm-6 col-md-6 col-lg-3 card-grid">' +
				'<a class="card-link" href="/rea/' + data.slug +'">'+
				'<div class="card" style="border-bottom: 4px solid ' + data.color + ';">'+
						'<img src="' + data.imagen + '" alt="'+ data.titulo +'">'+
						'<div class="card-block">'+
								'<div class="title-recurso">'+
										'<h4 class="card-title">' + data.titulo +'</h4>'+
								'</div>'+
								'<div class="content-excerpt">' +
									'<p class="card-text">' + data.descripcion + '</p>'+
								'</div>'+
						'</div>'+
						'<div class="category-recurso">'+
							'<p>' + data.categoria + '</p>'+
						'</div>'+
				'</div>'+
				'</a>'+
		'</div>';

		return recurso;
	}

	function closeNav() {
		$('.cd-nav-trigger').removeClass('nav-is-visible');
		$('.cd-main-header').removeClass('nav-is-visible');
		$('.cd-primary-nav').removeClass('nav-is-visible');
		$('.recursos-home.ficha').css("z-index",100);
		// $('.has-children ul').addClass('is-hidden');
		// $('.has-children a').removeClass('selected');
		$('.moves-out').removeClass('moves-out');
		$('.cd-main-content').removeClass('nav-is-visible').one('webkitTransitionEnd otransitionend oTransitionEnd msTransitionEnd transitionend', function(){
			$('body').removeClass('overflow-hidden');
		});
	}

	function toggleSearch(type) {
		if(type=="close") {
			//close serach
			$('.cd-search').removeClass('is-visible');
			$('.cd-search-trigger').removeClass('search-is-visible');
			$('.cd-overlay').removeClass('search-is-visible');
		} else {
			//toggle search visibility
			$('.cd-search').toggleClass('is-visible');
			$('.cd-search-trigger').toggleClass('search-is-visible');
			$('.cd-overlay').toggleClass('search-is-visible');
			if($(window).width() > MqL && $('.cd-search').hasClass('is-visible')) $('.cd-search').find('input[type="search"]').focus();
			($('.cd-search').hasClass('is-visible')) ? $('.cd-overlay').addClass('is-visible') : $('.cd-overlay').removeClass('is-visible') ;
		}
	}

	function checkWindowWidth() {
		//check window width (scrollbar included)
		var e = window,
            a = 'inner';
        if (!('innerWidth' in window )) {
            a = 'client';
            e = document.documentElement || document.body;
        }
        if ( e[ a+'Width' ] >= MqL ) {
			return true;
		} else {
			return false;
		}
	}

	function moveNavigation(){
		var navigation = $('.cd-nav');
  		var desktop = checkWindowWidth();
        if ( desktop ) {
			navigation.detach();
			navigation.insertBefore('.cd-header-buttons');
		} else {
			navigation.detach();
			navigation.insertAfter('.cd-main-content');
		}
	}


	function ellipsis_box(elemento, max_chars)
	{
		$(elemento).each(function(){
			limite_text = $(this).text();
			if (limite_text.length > max_chars)
			{
				limite = limite_text.substr(0, max_chars)+"...";
				$(this).text(limite);
			}
		});

	}

	function limitarTextos()
	{
		ellipsis_box(".card-title", 45);
		ellipsis_box(".card-block p", 40);
	}

	limitarTextos();


});
