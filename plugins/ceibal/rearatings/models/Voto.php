<?php namespace Ceibal\Rearatings\Models;

use Model;

/**
 * Model
 */
class Voto extends Model
{
    use \October\Rain\Database\Traits\Validation;
    
    /*
     * Validation
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ceibal_rearatings_recursos';
}