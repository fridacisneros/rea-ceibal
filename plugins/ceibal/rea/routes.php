<?php

    use Ceibal\Rea\Models\Tag;
    use Ceibal\Rea\Models\Recurso;

    Route::get('backend/ceibal/rea/api/recursos/{id?}', function($id = 0) {
        $availableTags = Tag::all()->lists('nombre');
        $assignedTags  = Tag::whereHas('recursos', function($q) use ($id) {
            $q->where('id', $id);
        })->lists('nombre');

        $response = [
            'assignedTags'  => $assignedTags,
            'availableTags' => $availableTags
        ];
        return Response::json($response);
    });

    Route::get('backend/ceibal/rea/api/destacados', function() {
        $recursosDestacados = Recurso::join('ceibal_rea_destacado', 'ceibal_rea_destacado.recurso_id','=','ceibal_rea_recursos.id')->where('principal',0)->publicados()->limit(8)->orderby('ceibal_rea_destacado.destacado_id','DESC')->get();
        $arrDestacados = [];
        foreach ($recursosDestacados as $recurso)
        {
          $rec              = new stdClass();
          $rec->titulo      = $recurso->titulo;
          $rec->descripcion = strip_tags($recurso->descripcion);
          $rec->imagen      = $recurso->imagen->getThumb(300,200,['mode' => 'crop']);
          $rec->slug        = $recurso->slug;
          $rec->categoria   = $recurso->categoria->nombre;
          $rec->color       = $recurso->categoria->color;

          $arrDestacados[]  = $rec;
        }

        $response = [
            'recursosDestacados'  => $arrDestacados
        ];
        return Response::json($response);
    });

    Route::get('backend/ceibal/rea/api/masvistos', function() {
        $recursosDestacados = Recurso::where('visitas','>',0)->orderBy('visitas','desc')->publicados()->limit(8)->get();

        $arrDestacados = [];
        foreach ($recursosDestacados as $recurso)
        {
          $rec              = new stdClass();
          $rec->titulo      = $recurso->titulo;
          $rec->descripcion = strip_tags($recurso->descripcion);
          $rec->imagen      = $recurso->imagen->getThumb(300,200,['mode' => 'crop']);
          $rec->slug        = $recurso->slug;
          $rec->categoria   = $recurso->categoria->nombre;
          $rec->color       = $recurso->categoria->color;

          $arrDestacados[]  = $rec;
        }

        $response = [
            'recursosDestacados'  => $arrDestacados
        ];
        return Response::json($response);
    });

    Route::get('backend/ceibal/rea/api/ultimos', function() {
        $recursosDestacados = Recurso::orderBy('published_on','desc')->publicados()->take(8)->get();

        $arrDestacados = [];
        foreach ($recursosDestacados as $recurso)
        {
          $rec              = new stdClass();
          $rec->titulo      = $recurso->titulo;
          $rec->descripcion = strip_tags($recurso->descripcion);
          $rec->imagen      = $recurso->imagen->getThumb(300,200,['mode' => 'crop']);
          $rec->slug        = $recurso->slug;
          $rec->categoria   = $recurso->categoria->nombre;
          $rec->color       = $recurso->categoria->color;

          $arrDestacados[]  = $rec;
        }

        $response = [
            'recursosDestacados'  => $arrDestacados
        ];
        return Response::json($response);
    });

?>
