<?php

    namespace Ceibal\Rea\FormWidgets;

    use Backend\Classes\FormWidgetBase;
    use Config;
    use Ceibal\Rea\Models\Tag;

    class Coleccion extends FormWidgetBase {

        public function widgetDetails() {
            return [
                'name'        => 'Coleccion',
                'description' => 'Buscador'
            ];
        }

        public function render() {
            $this->prepareVars();
            return $this->makePartial('widgets');
        }

        public function prepareVars() {
            $this->vars['id'] = $this->model->id;
        }

        // public function loadAssets() {
        //     $this->addCss('/plugins/ceibal/rea/assets/css/jquery.taghandler.css');
        //     $this->addCss('/plugins/ceibal/rea/assets/css/jquery-ui.min.css');
        //     $this->addJs('/plugins/ceibal/rea/assets/js/jquery-ui.min.js');
        //     $this->addJs('/plugins/ceibal/rea/assets/js/jquery.taghandler.min.js');
        // }

        // public function getSaveValue($value) {
        //     $tags = explode(",", implode(",", $value));
        //     $ids  = [];
        //     foreach($tags as $name) {
        //         if(empty($name)) { continue; }
        //         $created = Tag::where('nombre',$name)->first();
        //         if (!is_null($created)){
        //             $ids[] = $created->id;
        //         }
        //     }
        //     return $ids;
        // }

    }

?>
