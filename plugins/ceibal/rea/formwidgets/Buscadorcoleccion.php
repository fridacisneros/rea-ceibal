<?php

    namespace Ceibal\Rea\FormWidgets;

    use Backend\Classes\FormWidgetBase;
    use Config;
    use Ceibal\Rea\Models\Tag;

    class Buscadorcoleccion extends FormWidgetBase {

        public function widgetDetails() {
            return [
                'name'        => 'Buscador',
                'description' => 'Buscador de colecciones.'
            ];
        }

        public function render() {
            // $this->prepareVars();
            return $this->makePartial('widgets');
        }

        // public function prepareVars() {
        //     $this->vars['id'] = $this->model->id;
        // }

        // public function loadAssets() {
        //     $this->addJs('/plugins/ceibal/rea/assets/js/jquery-ui.min.js');
        //     $this->addJs('/plugins/ceibal/rea/assets/js/jquery.taghandler.min.js');
        // }


    }

?>
