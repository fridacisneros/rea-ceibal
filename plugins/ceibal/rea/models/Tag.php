<?php namespace Ceibal\Rea\Models;

use Model;

/**
 * Tag Model
 */
class Tag extends Model
{
    use \October\Rain\Database\Traits\Sluggable;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ceibal_rea_tags';

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['nombre'];

    /** @var array  */
    protected $slugs    = ['slug' => 'nombre'];


    /** @var array Relations */
    public $belongsToMany = [
        'recursos' => ['Ceibal\Rea\Models\Recurso', 'table' => 'ceibal_rea_tag_recurso', 'timestamps' => true]
    ];

    public function scopeListTags($query, $sortOrder) {
        $sortOrder = explode(' ', $sortOrder);
        $sortedBy  = $sortOrder[0];
        $direction = $sortOrder[1];
        return $query->orderBy($sortedBy, $direction);
    }
}
