<?php
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 10/2/17
 * Time: 10:40 AM
 */

namespace Ceibal\Rea\Models;

use Model;

class Categoria extends Model {

    use \October\Rain\Database\Traits\NestedTree;
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\Purgeable;
    use \October\Rain\Database\Traits\Sluggable;

    public $table = 'ceibal_rea_categorias';

    /**
     * @var array Generate slugs for these attributes.
     */
    protected $slugs = ['slug' => 'nombre'];

    public $hasMany = [
        'recursos' => 'Ceibal\Rea\Models\Recurso'
    ];

    public $hasManyThrough = [
        'recursosHijos' => [
            'Ceibal\Rea\Models\Recurso',
            'key'        => 'parent_id',
            'through'    => 'Ceibal\Rea\Models\Categoria',
            'throughKey' => 'categoria_id'
        ],
    ];

    public $rules = [
        'nombre' => 'required',
        'color' => 'required'
    ];


    protected $fillable  = ['descripcion', 'parent_id'];
    protected $purgeable = [];

    private $controller;

    public function getSelectList() {
        $cats = $this->where('parent_id', null)->get();
        $output = array();
        foreach($cats as $cat) {
            $depthIndicator = $this->getDepthIndicators($cat->nest_depth);
            $output["id-$cat->id"] = $depthIndicator . ' ' . $cat->descripcion;
        }
        return $output;
    }

    protected function getDepthIndicators($depth = 0, $indicators = '') {
        if($depth < 1) {
            return $indicators;
        }
        return $this->getDepthIndicators(--$depth, $indicators . '-');
    }

    public static function getClassName() {
        return get_called_class();
    }

}

?>
