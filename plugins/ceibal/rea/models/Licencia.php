<?php namespace Ceibal\Rea\Models;

use Model;

/**
 * Licencia Model
 */
class Licencia extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'ceibal_rea_licencias';

    /**
     * @var array Fillable fields
     */
    protected $fillable = ['nombre', 'tipo', 'url'];

    /*
     * Validation
     */
    public $rules = [
        'nombre'    => 'required|max:150',
        'tipo'      => 'required|max:10',
        'url'       => 'required|url'
    ];

    /**
     * @var array Relations
     */
    public $hasMany = [
        'recursos' => 'Ceibal\Rea\Models\Recurso'
    ];

    public $attachOne = [
        'imagen' => 'System\Models\File'
    ];
}
