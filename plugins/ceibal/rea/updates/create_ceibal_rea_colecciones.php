<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCeibalReaColecciones extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('ceibal_rea_coleccion'))
        {
            Schema::create('ceibal_rea_coleccion', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('titulo', 150)->unique();
                $table->string('slug', 100);
                $table->softDeletes();
                $table->timestamps();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('ceibal_rea_coleccion');
    }
}
