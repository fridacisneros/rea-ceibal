<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCeibalReaColeccionRecurso extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('ceibal_rea_coleccion_recurso'))
        {
            Schema::create('ceibal_rea_coleccion_recurso', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('recurso_id');
                $table->integer('coleccion_id');
                $table->timestamp('created_at')->nullable();
                $table->timestamp('updated_at')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::dropIfExists('ceibal_rea_coleccion_recurso');
    }
}
