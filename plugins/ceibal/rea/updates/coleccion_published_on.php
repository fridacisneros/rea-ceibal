<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class ColeccionPublishedOn extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('ceibal_rea_coleccion','published'))
        {
            Schema::table('ceibal_rea_coleccion', function($table)
            {
                $table->boolean('published')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('ceibal_rea_coleccion', function ($table) {
            $table->dropColumn('published');
        });
    }
}