<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateCeibalReaColeccionBusqueda extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('ceibal_rea_coleccion','buscador'))
        {
            Schema::table('ceibal_rea_coleccion', function($table)
            {
                $table->text('buscador')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('ceibal_rea_coleccion', function ($table) {
            $table->dropColumn('buscador');
        });
    }
}
