<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateCeibalReaTables extends Migration
{
    public function up()
    {
        if (!Schema::hasTable('ceibal_rea_recursos'))
        {
            Schema::create('ceibal_rea_recursos', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('titulo', 150)->unique();
                $table->string('slug', 100);
                $table->boolean('principal')->default(0);
                $table->boolean('destacado')->default(0);
                $table->text('descripcion'  )->nullable();
                $table->text('propositos'  )->nullable();
                $table->integer('nivel')->unsigned()->default(0);
                $table->string('url_elp'  , 100);
                $table->string('url_zip', 100);
                $table->string('url_visualizar'  , 100)->nullable();
                $table->integer('licencia_id')->unsigned()->nullable()->index();
                $table->boolean('publicado' );
                $table->integer('preview')->nullable();
                $table->softDeletes();
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('ceibal_rea_categorias'))
        {
            Schema::create('ceibal_rea_categorias', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->integer('parent_id')->unsigned()->index()->nullable();
                $table->string('nombre', 150);
                $table->string('slug', 100);
                $table->text('descripcion')->nullable();
                $table->string('color', 10);
                $table->boolean('oculto')->default(0);
                $table->integer('nest_left' )->unsigned()->nullable();
                $table->integer('nest_right')->unsigned()->nullable();
                $table->integer('nest_depth')->nullable();
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('ceibal_rea_tags'))
        {
            Schema::create('ceibal_rea_tags', function($table)
            {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('nombre', 50);
                $table->string('slug'  , 25)->index();
                $table->timestamps();
            });
        }

        if (!Schema::hasTable('ceibal_rea_categoria_recurso'))
        {
            Schema::create('ceibal_rea_categoria_recurso', function($table)
            {
                $table->integer('categoria_id')->unsigned();
                $table->integer('recurso_id')->unsigned();
                $table->primary(['categoria_id', 'recurso_id']);
                $table->timestamps();
                $table->foreign('categoria_id')->references('id')->on('ceibal_rea_categorias')->onDelete('restrict');
                $table->foreign('recurso_id'  )->references('id')->on('ceibal_rea_recursos'  )->onDelete('restrict');
            });
        }

        if (!Schema::hasTable('ceibal_rea_tag_recurso'))
        {
            Schema::create('ceibal_rea_tag_recurso', function($table) {
                $table->integer('tag_id')->unsigned();
                $table->integer('recurso_id')->unsigned();
                $table->primary(['tag_id', 'recurso_id']);
                $table->timestamps();
                $table->foreign('tag_id'    )->references('id')->on('ceibal_rea_tags'    )->onDelete('restrict');
                $table->foreign('recurso_id')->references('id')->on('ceibal_rea_recursos')->onDelete('restrict');
            });
        }

        if (!Schema::hasTable('ceibal_rea_licencias'))
        {
            Schema::create('ceibal_rea_licencias', function($table) {
                $table->engine = 'InnoDB';
                $table->increments('id');
                $table->string('nombre');
                $table->string('tipo');
                $table->string('url');
                $table->softDeletes();
                $table->timestamps();
            });
        }

    }

    public function down() {
        Schema::dropIfExists('ceibal_rea_tag_recurso');
        Schema::dropIfExists('ceibal_rea_categoria_recurso');
        Schema::dropIfExists('ceibal_rea_recursos');
        Schema::dropIfExists('ceibal_rea_tags');
        Schema::dropIfExists('ceibal_rea_categorias');
        Schema::dropIfExists('ceibal_rea_licencias');
    }
}
