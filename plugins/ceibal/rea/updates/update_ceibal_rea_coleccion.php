<?php namespace Ceibal\Rea\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class UpdateCeibalReaColeccion extends Migration
{
    public function up()
    {
        if (!Schema::hasColumn('ceibal_rea_coleccion','descripcion'))
        {
            Schema::table('ceibal_rea_coleccion', function($table)
            {
                $table->text('descripcion')->nullable();
            });
        }
    }

    public function down()
    {
        Schema::table('ceibal_rea_coleccion', function ($table) {
            $table->dropColumn('descripcion');
        });
    }
}
