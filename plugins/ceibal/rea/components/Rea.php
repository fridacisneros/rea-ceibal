<?php namespace Ceibal\Rea\Components;
/**
 * Created by PhpStorm.
 * User: pablo
 * Date: 10/2/17
 * Time: 2:02 PM
 */

use Cms\Classes\ComponentBase;

class Rea extends ComponentBase
{

    /**
     * Returns information about this component, including name and description.
     */
    public function componentDetails()
    {
        return [
            'name' => 'Rea',
            'description' => 'Componente para trabajar con los Recursos Educativos Abiertos.'
        ];
    }

    public function recursos()
    {
        return ['Primer recurso', 'Segundo recurso'];
    }

}