<?php namespace Ceibal\Rea\Components;

use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Ceibal\Rea\Models\Coleccion;

class Colecciones extends ComponentBase
{
    /** @var  Variable para listar los Recursos */
    public $colecciones;

    public $resourcePage;
    public $maxItems;
    public $filtro;
    public $pageParam;

    public function componentDetails()
    {
        return [
            'name'        => 'Listado de colecciones',
            'description' => 'Componente para mostrar un listado de colecciones'
        ];
    }

    public function defineProperties()
    {
        return [
            'resourcePage'  => [
                'title'             => 'Página del recurso',
                'description'       => 'Define cuál va a ser la URL asociada a los elementos listados',
                'type'              => 'dropdown',
                'default'           => 'rea/Recurso'
            ],
            'filtro'        => [
                'title'             => 'Filtrar por',
                'description'       => 'Define por qué tipo de elemento se desea filtrar',
                'type'              => 'dropdown',
                'default'           => 'none',
                'options'           => ['none' => 'Sin filtro', 'category' => 'Categorías', 'tag' => 'Tags']
            ],
            'maxItems'      => [
                'title'             => 'Cantidad máxima',
                'description'       => 'La cantidad máxima de elementos en la página',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'El valor ingresado debe ser numérico'
            ],
            'pageNumber'    => [
                'title'             => 'Número de la página',
                'description'       => 'Utilizado para indicar cuál es la página actual',
                'type'              => 'string',
                'default'           => '{{ :page }}',
            ]
        ];
    }

    public function getResourcePageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->pageParam    = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->resourcePage = $this->page['resourcePage'] = $this->property('resourcePage');
        $this->filtro       = $this->page['filtro'] = $this->property('filtro');
        $this->maxItems     = $this->page['maxItems'] = $this->property('maxItems');

        $colecciones = Coleccion::where('published', 1)->orderby('titulo')->paginate($this->maxItems);


        foreach ($colecciones as $key => $coleccion)
        {
            $coleccion->descripcion = strip_tags($coleccion->descripcion);
        }

        $this->colecciones  = $this->page['colecciones'] = $colecciones;



    }
}
