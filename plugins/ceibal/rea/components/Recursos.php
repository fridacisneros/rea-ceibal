<?php namespace Ceibal\Rea\Components;

use Ceibal\Rea\Models\Categoria;
use Ceibal\Rea\Models\Tag;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Ceibal\Rea\Models\Recurso as RecursoRea;

class Recursos extends ComponentBase
{
    /** @var  Variable para listar los Recursos */
    public $posts;

    public $resourcePage;
    public $maxItems;
    public $filtro;
    public $pageParam;

    public function componentDetails()
    {
        return [
            'name'        => 'Listado recursos',
            'description' => 'Componente para mostrar un listado de recursos'
        ];
    }

    public function defineProperties()
    {
        return [
            'resourcePage'  => [
                'title'             => 'Página del recurso',
                'description'       => 'Define cuál va a ser la URL asociada a los elementos listados',
                'type'              => 'dropdown',
                'default'           => 'rea/Recurso'
            ],
            'filtro'        => [
                'title'             => 'Filtrar por',
                'description'       => 'Define por qué tipo de elemento se desea filtrar',
                'type'              => 'dropdown',
                'default'           => 'none',
                'options'           => ['none' => 'Sin filtro', 'category' => 'Categorías', 'tag' => 'Tags']
            ],
            'maxItems'      => [
                'title'             => 'Cantidad máxima',
                'description'       => 'La cantidad máxima de elementos en la página',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'El valor ingresado debe ser numérico'
            ],
            'pageNumber'    => [
                'title'             => 'Número de la página',
                'description'       => 'Utilizado para indicar cuál es la página actual',
                'type'              => 'string',
                'default'           => '{{ :page }}',
            ]
        ];
    }

    public function getResourcePageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->pageParam    = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->resourcePage = $this->page['resourcePage'] = $this->property('resourcePage');
        $this->filtro       = $this->page['filtro'] = $this->property('filtro');
        $this->maxItems     = $this->page['maxItems'] = $this->property('maxItems');

        $subcat = "";
        $this->page["subcat"] = "";
        $classBtn1 = "";
        $classBtn2 = "";
        $classBtn3 = "";

        $selectedBtn1 = "";
        $selectedBtn2 = "";
        $selectedBtn3 = "";

        if(isset($_GET["c"]))
        {
            $subcat = $_GET["c"];
            $this->page["subcat"] = $subcat;

            switch ($subcat) {
              case 1:
                $classBtn1 = "active";
                $selectedBtn1 = "selected";
              break;
              case 2:
                $classBtn2 = "active";
                $selectedBtn2 = "selected";
              break;
              case 3:
                $classBtn3 = "active";
                $selectedBtn3 = "selected";
              break;
            }
        }else
        {
          $classBtn3 = "active";
        }

        $this->page["classBtn1"] = $classBtn1;
        $this->page["classBtn2"] = $classBtn2;
        $this->page["classBtn3"] = $classBtn3;

        $this->page["selectedBtn1"] = $selectedBtn1;
        $this->page["selectedBtn2"] = $selectedBtn2;
        $this->page["selectedBtn3"] = $selectedBtn3;

        switch ($this->filtro){
            case 'none':

                if($subcat && ($subcat != 3))
                {
                    $this->posts = $this->page['posts'] = RecursoRea::nivel($subcat)->publicados()->paginate($this->maxItems);
                }else {
                    $this->posts = $this->page['posts'] = RecursoRea::publicados()->paginate($this->maxItems);
                }

                break;
            case 'category':
                $categoria  = Categoria::where('slug', $this->param('slug'))->first();

                $allCats    = Categoria::where('parent_id',$categoria->id)->get();

                $recursos   = RecursoRea::where(function ($query) use($categoria,$allCats)
                {
                                $query->where('categoria_id', $categoria->id);
                                foreach ($allCats as $key => $value)
                                {
                                    $query->orWhere('categoria_id', $value->id);
                                }

                            });

                if ($categoria->nest_depth == 0)
                {

                    if($subcat && ($subcat != 3))
                    {

                    $this->posts = $this->page['posts'] = $recursos->publicados()->nivel($subcat)->publicados()->paginate($this->maxItems);
                    }else {
                      $this->posts =  $this->page['posts'] = $recursos->publicados()->paginate($this->maxItems);////$this->page['posts'] = $categoria->recursosHijos()->publicados()->paginate($this->maxItems);
                    }
                }
                else
                {
                    if($subcat && ($subcat != 3))
                    {
                     $this->posts = $this->page['posts'] = $categoria->recursos()->nivel($subcat)->publicados()->paginate($this->maxItems);
                    }else {
                      $this->posts = $this->page['posts'] = $categoria->recursos()->publicados()->paginate($this->maxItems);
                    }
                }

            break;
            case 'tag':
                $tag = Tag::where('slug', $this->param('slug'))->first();

                if($subcat && ($subcat != 3))
                {
                  $this->posts = $this->page['posts'] = $tag->recursos()->nivel($subcat)->publicados()->limit($this->maxItems)->get();
                }else {
                  $this->posts = $this->page['posts'] = $tag->recursos()->publicados()->limit($this->maxItems)->get();
                }

                break;
        }

        foreach ($this->posts as $recurso) {
            $recurso->descripcion = strip_tags($recurso->descripcion);
        }
    }
}
