<?php namespace Ceibal\Rea\Components;

use Cms\Classes\ComponentBase;
use Ceibal\Rea\Models\RecursosExternos AS RecursoExterno;
use Cms\Classes\Page;

class RecursosExternos extends ComponentBase
{
    public $resource;
    public $pageParam;
    public $resourcePage;


    public function componentDetails()
    {
        return [
            'name'        => 'Recursos Externos',
            'description' => 'Visualización y listado de recursos externos'
        ];
    }

    public function defineProperties()
    {
        return [
            'resourcePage'  => [
                'title'             => 'Página del recurso',
                'description'       => 'Define cuál va a ser la URL asociada a los elementos listados',
                'type'              => 'dropdown',
                'default'           => 'rea/Recursos'
            ],
            'pageNumber'    => [
                'title'             => 'Número de la página',
                'description'       => 'Utilizado para indicar cuál es la página actual',
                'type'              => 'string',
                'default'           => '{{ :page }}',
            ]
        ];
    }

    public function getResourcePageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }



    public function onRun()
    {
        $this->resourcePage = $this->page['resourcePage'] = $this->property('resourcePage');

        $this->resource = $this->page['recursosexternos'] = RecursoExterno::orderby('created_at', 'desc')->paginate(8);

        foreach ($this->resource as $key => $recursoexterno)
        {
            $recursoexterno->description = str_replace("&nbsp;", " ",strip_tags($recursoexterno->description));
        }

    }
}
