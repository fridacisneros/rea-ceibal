<?php namespace Ceibal\Rea\Components;

use Ceibal\Rea\Models\Categoria;
use Ceibal\Rea\Models\Tag;
use Cms\Classes\Page;
use Cms\Classes\ComponentBase;
use Ceibal\Rea\Models\Recurso as RecursoRea;

class Relacionados extends ComponentBase
{
    /** @var  Variable para listar los Recursos */
    public $posts;

    public $resourcePage;
    public $maxItems;
    public $filtro;
    public $pageParam;

    public function componentDetails()
    {
        return [
            'name'        => 'Recursos relacionados',
            'description' => 'Componente para mostrar un listado de recursos relacionados'
        ];
    }

    public function defineProperties()
    {
        return [
            'resourcePage'  => [
                'title'             => 'Página del recurso',
                'description'       => 'Define cuál va a ser la URL asociada a los elementos listados',
                'type'              => 'dropdown',
                'default'           => 'rea/Recurso'
            ],
            'filtro'        => [
                'title'             => 'Filtrar por',
                'description'       => 'Define por qué tipo de elemento se desea filtrar',
                'type'              => 'dropdown',
                'default'           => 'none',
                'options'           => ['none' => 'Sin filtro', 'category' => 'Categorías', 'tag' => 'Tags']
            ],
            'maxItems'      => [
                'title'             => 'Cantidad máxima',
                'description'       => 'La cantidad máxima de elementos en la página',
                'default'           => 10,
                'type'              => 'string',
                'validationPattern' => '^[0-9]+$',
                'validationMessage' => 'El valor ingresado debe ser numérico'
            ]
        ];
    }

    public function getResourcePageOptions()
    {
        return Page::sortBy('baseFileName')->lists('baseFileName', 'baseFileName');
    }

    public function onRun()
    {
        $this->pageParam    = $this->page['pageParam'] = $this->paramName('pageNumber');
        $this->resourcePage = $this->page['resourcePage'] = $this->property('resourcePage');
        $this->filtro       = $this->page['filtro'] = $this->property('filtro');
        $this->maxItems     = $this->page['maxItems'] = $this->property('maxItems');

        switch ($this->filtro){
            case 'none':
                $this->posts = $this->page['posts'] = RecursoRea::publicados()->paginate($this->maxItems);
                break;
            case 'category':
                $categoria = Categoria::find($this->param('categoria_id'));
                $this->posts = $this->page['posts'] = $categoria->recursos()->publicados()->paginate($this->maxItems);
                break;
            case 'tag':
                // OBTENGO MAXITEMS RECURSOS RELACIONADOS ORDENADOS ALEATORIAMENTE
                $this->posts = $this->page['posts'] = $this->page['post']->relacionados()->publicados()->limit($this->maxItems)->get()->shuffle();
                break;
        }

        foreach ($this->posts as $recurso) {
            $recurso->descripcion = strip_tags($recurso->descripcion);
        }
    }
}
