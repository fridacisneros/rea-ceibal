<?php namespace Ceibal\Rea;

use Backend;
use BackendAuth;
use System\Classes\PluginBase;
use Ceibal\Rea\Models\Recurso;
use Ceibal\Rea\Models\Categoria;
use Ceibal\Rea\Models\Tag;
use Ceibal\Rea\Models\Coleccion;
use Ceibal\Faqs\Models\Faq;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'          => 'REA Plugin',
            'description'   => 'Exelearning functionality',
            'author'        => 'Pablo Perdomo',
            'icon'          => 'icon-leaf'
        ];
    }

    public $require = ['offline.sitesearch', 'martin.ssbuttons'];

    public function registerNavigation() {

        return [
            'rea' => [
                'label'       => 'REA',
                'url'         => Backend::url('ceibal/rea/recursos'),
                'icon'        => 'icon-briefcase',
                'permissions' => ['ceibal.rea.access'],
                'order'       => 1,

                'sideMenu'    => [
                    'recursos'    => [
                        'label'       => 'Bandeja de entrada',
                        'icon'        => 'icon-inbox',
                        'url'         => Backend::url('ceibal/rea/recursos'),
                        'permissions' => ['ceibal.rea.access']
                    ],
                    'misrecursos'    => [
                        'label'       => 'Todos los recursos',
                        'icon'        => 'icon-folder',
                        'url'         => Backend::url('ceibal/rea/recursosusuario'),
                        'permissions' => ['ceibal.rea.access_colecciones']
                    ],
                    'categorias'    => [
                        'label'       => 'Categorias',
                        'icon'        => 'icon-check-square-o',
                        'url'         => Backend::url('ceibal/rea/categorias'),
                        'permissions' => ['ceibal.rea.access_categorias']
                    ],
                    'reorder' => [
                        'label'       => 'Reordenar Categorias',
                        'icon'        => 'icon-exchange',
                        'url'         => Backend::url('ceibal/rea/categorias/reorder'),
                        'permissions' => ['ceibal.rea.reorder_categorias']
                    ],
                    'licencias' => [
                        'label'       => 'Licencias CC',
                        'icon'        => 'icon-creative-commons',
                        'url'         => Backend::url('ceibal/rea/licencias'),
                        'permissions' => ['ceibal.rea.licencias']
                    ],
                    'colecciones'    => [
                        'label'       => 'Colecciones',
                        'icon'        => 'icon-object-group',
                        'url'         => Backend::url('ceibal/rea/colecciones'),
                        'permissions' => ['ceibal.rea.access_colecciones']
                    ],
                    'recursos_externos'    => [
                        'label'       => 'Recursos Externos',
                        'icon'        => 'icon-list-alt',
                        'url'         => Backend::url('ceibal/rea/recursosexterno'),
                        'permissions' => ['ceibal.rea.access']
                    ],
                    'exportar'    => [
                        'label'       => 'Exportar Recursos',
                        'url'         => Backend::url('ceibal/rea/recursos/export'),
                        'icon'        => 'icon-download',
                        'permissions' => ['ceibal.rea.access']
                    ],
                ]
            ]
        ];
    }

    public function registerPermissions() {
        return [
            'ceibal.rea.access' => [
                'tab'   => 'REA',
                'label' => 'Acceso al backend de REA',
            ],
            'ceibal.rea.access_categorias' => [
                'tab'   => 'REA',
                'label' => 'Acceso a categorías',
            ],
            'ceibal.rea.reorder_categorias' => [
                'tab'   => 'REA',
                'label' => 'Reordenar categorías',
            ],
            'ceibal.rea.licencias' => [
                'tab'   => 'REA',
                'label' => 'Cargar y administrar licencias CC',
            ],
            'ceibal.rea.access_colecciones' => [
                'tab'   => 'REA',
                'label' => 'Acceso a colecciones',
            ],
        ];
    }

    public function registerComponents()
    {
        return [
            'Ceibal\Rea\Components\Rea'     => 'rea',
            'Ceibal\Rea\Components\Recurso' => 'recurso',
            'Ceibal\Rea\Components\Ranking' => 'ranking',
            'Ceibal\Rea\Components\Recursos' => 'recursos',
            'Ceibal\Rea\Components\Buscador' => 'buscador',
            'Ceibal\Rea\Components\Coleccion' => 'coleccion',
            'Ceibal\Rea\Components\Resultados' => 'resultados',
            'Ceibal\Rea\Components\Colecciones' => 'colecciones',
            'Ceibal\Rea\Components\Relacionados' => 'relacionados',
            'Ceibal\Rea\Components\RecursoExterno' => 'recursoexterno',
            'Ceibal\Rea\Components\RecursosExternos' => 'recursosexternos',
        ];
    }

    public function registerFormWidgets() {
        return [
            'Ceibal\Rea\FormWidgets\Tagbox' => [
                'code'  => 'tagbox',
                'label' => 'Tags'
            ],
            'Ceibal\Rea\FormWidgets\Coleccion' => [
                'code'  => 'coleccion',
                'label' => 'Buscador de recurso'
            ],
            'Ceibal\Rea\FormWidgets\Buscadorcoleccion' => [
                'code'  => 'buscador',
                'label' => 'Buscador de colección'
            ]
        ];
    }

    public function boot()
    {
        \Event::listen('offline.sitesearch.query', function ($query)
        {
                $this->query = $query;

                $q = "";
                $a = "";
                $c = "";
                $n = "";
                $e = "";
                $cc = "";

                if(isset($_REQUEST["q"]))
                {
                    $q = $_REQUEST["q"];
                }

                if(isset($_REQUEST["a"]))
                {
                    $a = $_REQUEST["a"];
                }

                if(isset($_REQUEST["c"]))
                {
                    $c = $_REQUEST["c"];
                }

                if(isset($_REQUEST["n"]))
                {
                    $n = $_REQUEST["n"];
                }

                if(isset($_REQUEST["e"]))
                {
                    $e = $_REQUEST["e"];
                }

                if(isset($_REQUEST["cc"]))
                {
                    $cc = $_REQUEST["cc"];
                }




                $items          = Recurso::publicados();
                $colecciones;   
                $faqs = [];

                if($q)
                {
                    $items =$items->where(function($qf) use ($q){
                        $search = explode(" ",$q);
                        if (is_array($search))
                        {
                            foreach ($search as $key => $value)
                            {
                                $qf->where('titulo', 'like', "%$value%")
                                    ->orWhere('descripcion','like',"%$value%")
                                    ->orWhere('propositos','like',"%$value%");
                            }
                        }
                    });

                    $colecciones = Coleccion::where('published',1)
                        ->where(function($qc) use ($q){
                            $search = explode(" ",$q);
                            if (is_array($search))
                            {
                                foreach ($search as $key => $value)
                                {
                                    $qc->where('titulo', 'like', "%$value%")
                                        ->orWhere('descripcion','like',"%$value%");
                                }
                            }
                    });

                        $faqs = Faq::where(function($q){
                            $q->where('title', 'like', "%$this->query%")
                                ->orWhere('description','like',"%$this->query%");
                        })->get();
                }

                if($a)
                {
                  $items = $items->where('autor_name','like',"%$a%");
                }

                if($c)
                {
                    $items = $items->where('categoria_id', $c);
                }

                if($n)
                {
                    $items = $items->where(function($query) use($n){
                            $query->where('nivel', $n)
                            ->orWhere('nivel',3);
                    });
                }

                if($e)
                {
                    $items = $items->join('ceibal_rea_tag_recurso','ceibal_rea_tag_recurso.recurso_id','=','ceibal_rea_recursos.id')
                    ->where('ceibal_rea_tag_recurso.tag_id',$e);
                }


                if($cc)
                {
                    if($q)
                    {
                        $items = Coleccion::where('id',$cc)->first()->recursos()->where('titulo','like','%'.$q.'%');
                        $colecciones = null;
                    }else {
                        $colecciones = Coleccion::where('id',$cc)->first()->recursos();

                    }

                }else {
                    if($q)
                    {
                        $colecciones = $colecciones->where('titulo','like','%'.$cc.'%');
                    }else {
                        $colecciones = Coleccion::where('id',$cc);
                    }
                }

                if($q || $a || $c || $n || $e)
                {
                    $items = $items->get();
                }else {
                    $items = null;
                }

                if(isset($colecciones))
                {
                        $colecciones = $colecciones->get();
                        if($items)
                        {
                            $items = $items->merge($colecciones);
                        }else {
                            $items = $colecciones;
                        }

                        $items = $items->merge($faqs);

                }


                // Now build a results array
                $results = $items->map(function ($item) use ($query) {

                    // If the query is found in the title, set a relevance of 2
                    $relevance = mb_stripos($item->titulo, $query) !== false ? 2 : 1;

                    $tit = $item->titulo;
                    $desc = strip_tags($item->descripcion);
                    if($item->categoria)
                    {
                        $url = '/rea/'.$item->slug;
                    }
                    elseif($item->description)
                    {
                        $url = "/faq/".$item->id;
                        $tit = $item->title;
                        $desc = strip_tags($item->description);
                    }
                    else {
                        $url = '/coleccion/'.$item->slug;
                    }

                    return [
                        'title'     => $tit,
                        'text'      => $desc,
                        'url'       => $url,//'/document/' . $item->slug,
                        'thumb'     => $item->imagen,//$item->images->first(), // Instance of System\Models\File
                        'relevance' => $relevance, // higher relevance results in a higher

                                                   // position in the results listing
                        // 'meta' => 'data',       // optional, any other information you want
                                                   // to associate with this result
                         'model' => $item,       // optional, pass along the original model
                    ];
                });

                return [
                    'provider' => 'Document', // The badge to display for this result
                    'results'  => $results,
                ];
        });
    }
}
