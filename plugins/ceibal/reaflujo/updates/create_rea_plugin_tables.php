<?php namespace Ceibal\ReaFlujo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CreateReaPluginTables extends Migration
{
    public function up()
    {
        echo "\n CreateReaPluginTables \n";

        try {
            \DB::statement('SET FOREIGN_KEY_CHECKS=0');
            self::create_tables();
        } catch (Exception $e) {
            \DB::statement('SET FOREIGN_KEY_CHECKS=1');
        }
    }

    private function create_tables()
    {
        /*
        Drop de las tablas si es que existen, el comando pluging:refresh no llama al metodo down()
        */
        if (Schema::hasTable('rea_plugin_log'))
        {
            Schema::drop('rea_plugin_log');
        }

        if (Schema::hasTable('rea_plugin_etapa_user_groups')) {
            Schema::drop('rea_plugin_etapa_user_groups');
        }

        if (Schema::hasTable('rea_plugin_etapa_user_roles')) {
            Schema::drop('rea_plugin_etapa_user_roles');
        }

        if (Schema::hasTable('rea_plugin_etapa')) {
            Schema::drop('rea_plugin_etapa');
        }

        /*
        Las diferntes etapas del flujo
        */
        Schema::create('rea_plugin_etapa', function($table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('nombre');
            $table->string('code');
            $table->string('estado');
            $table->string('accion')->nullable();
            $table->string('descripcion')->nullable();
            $table->timestamps();
        });

        /*
        Esta relacion corresponde al backend_user_roles y a el tipo de etapa, a q roles de usuario le corresponde que etapas
        */
        Schema::create('rea_plugin_etapa_user_roles', function($table){
            $table->engine = 'InnoDB';
            $table->integer('etapaId')->unsigned();
            $table->integer('roleId')->unsigned();
            $table->primary(['etapaId', 'roleId']);
            $table->foreign('etapaId')->references('id')->on('rea_plugin_etapa');
            $table->foreign('roleId')->references('id')->on('backend_user_roles');
            $table->timestamps();

        });

        /*
        Esta relacion corresponde al backend_user_group y a el tipo de etapa, a q groups de usuario le corresponde que etapas
        */
        Schema::create('rea_plugin_etapa_user_groups', function($table){
            $table->engine = 'InnoDB';
            $table->integer('etapaId')->unsigned();
            $table->integer('groupId')->unsigned();
            $table->primary(['etapaId', 'groupId']);
            $table->foreign('etapaId')->references('id')->on('rea_plugin_etapa');
            $table->foreign('groupId')->references('id')->on('backend_user_groups');
            $table->timestamps();
        });

        /*
        Esta relacion corresponde al flujo que articulo esta en q etapa y que usuario esta trabajando o trabajo en la misma.
        */
        Schema::create('rea_plugin_log',function($table){
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('etapaId');
            $table->integer('recursoId');
            $table->integer('usuarioId')->default(0);
            # $table->foreign('etapaId')->references('id')->on('rea_plugin_etapa');
            # $table->foreign('recursoId')->references('id')->on('ceibal_rea_recursos');
            $table->timestamps();
        });
    }

    public function down()
    {
        if (Schema::hasTable('rea_plugin_log'))
        {
            Schema::drop('rea_plugin_log');
        }

        if (Schema::hasTable('rea_plugin_etapa_user_groups'))
        {
            Schema::drop('rea_plugin_etapa_user_groups');
        }

        if (Schema::hasTable('rea_plugin_etapa_user_roles'))
        {
            Schema::drop('rea_plugin_etapa_user_roles');
        }

        if (Schema::hasTable('rea_plugin_etapa'))
        {
            Schema::drop('rea_plugin_etapa');
        }




    }
}
