<?php namespace Ceibal\ReaFlujo\Updates;

use Backend\Models\UserRole;
use Ceibal\ReaFlujo\Models\ReaPluginEtapa;
use Ceibal\ReaFlujo\Models\ReaPluginEtapaUserRoles;
use Seeder;

class ReaPluginEtapasUserRolesSeeder extends Seeder
{

	/*
	Esta funcion se encarga de insertar los roles correspondientes
	*/
	private function insertarRole($userRoleCode='', $etapasCode=[])
	{
		try
		{
			# obtengo la id del role
			$role = UserRole::where('code', $userRoleCode)->get();
			# Para cada role le asocio su etapa correspondiete
			foreach ($etapasCode as $etapaCode)
			{

				if(isset($role[0]->attributes['id'])){
					$etapa = ReaPluginEtapa::where('code', $etapaCode)->get();
					$etapaUserRole = ReaPluginEtapaUserRoles::firstOrCreate([
						'roleId'  => $role[0]->attributes['id'],
						'etapaId' => $etapa[0]->attributes['id']
					]);
					$etapaUserRole->created_at = date('Y-m-d H:i:s');
		        	$etapaUserRole->updated_at = date('Y-m-d H:i:s');
		        	$etapaUserRole->save();
				}

			}
		}catch(Exception $e)
		{
		}
	}

	public function run()
	{
        echo "\n ReaPluginEtapasUserRolesSeeder \n";
        $todasLasEtapas = [ 'etapaUno', 'etapaDos', 'etapaTres', 'etapaCuatro', 'etapaCinco'];

		$owner = 'developer';
		self::insertarRole($owner, $todasLasEtapas);

		$owner = 'publisher';
		self::insertarRole($owner, $todasLasEtapas);

		$owner = 'formacion';
		self::insertarRole($owner, $todasLasEtapas);

		$administradorGlobal = 'administradorGlobal';
		self::insertarRole($administradorGlobal, $todasLasEtapas);

		$correctorInstruccional = 'correctorInstruccional';
		self::insertarRole($correctorInstruccional, ['etapaDos']);

		$correctorDeEstilo = 'correctorDeEstilo';
		self::insertarRole($correctorDeEstilo, ['etapaTres']);

		$correctorDeDisenoGrafico = 'correctorDeDisenoGrafico';
		self::insertarRole($correctorDeDisenoGrafico, ['etapaCuatro']);


		$creadorEditorCorrectorYAprobadorDeRecursos = 'creadorEditorCorrectorYAprobadorDeRecursos';
		self::insertarRole($creadorEditorCorrectorYAprobadorDeRecursos, $todasLasEtapas);


		$contenidistaExterno = 'contenidistaExterno';
		self::insertarRole($contenidistaExterno, ['etapaUno']);

		$usuarioFinal = 'usuarioFinal';
		self::insertarRole($usuarioFinal, ['etapaUno']);
	}
}
