<?php namespace Ceibal\ReaFlujo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CeibalReaRecursoComentario extends Migration
{
    public function up()
    {
        Schema::table('ceibal_rea_recursos', function($table)
        {
            $table->text('comentario')->nullable();
        });
    }

    public function down()
    {

        $table->dropColumn('comentario');
    }
}
