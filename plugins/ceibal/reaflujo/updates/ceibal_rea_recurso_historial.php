<?php namespace Ceibal\ReaFlujo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CeibalReaRecursoHistorial extends Migration
{
    public function up()
    {
        Schema::table('ceibal_rea_recursos', function($table)
        {
            $table->text('historial')->nullable();
        });
    }

    public function down()
    {

        $table->dropColumn('historial');
    }
}
