<?php namespace Ceibal\ReaFlujo\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class CeibalReaRecursoLogComentario extends Migration
{
    public function up()
    {
        Schema::table('rea_plugin_log', function($table)
        {
            $table->text('comentario')->nullable();
        });
    }

    public function down()
    {

        $table->dropColumn('comentario');
    }
}
