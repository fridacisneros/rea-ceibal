<?php namespace Ceibal\ReaFlujo\Models;

use Model;
use Db;
class ReaPluginEtapa extends Model{
    public $table = 'rea_plugin_etapa';

    public static function obtenerEtapaId($code='')
    {
        $etapa = ReaPluginEtapa::where('code', $code)->get();
        return $etapa[0]->id;
    }
    public static function obtenerUserPorEtapaCode($code=''){

    	$users = Db::table('backend_users as bu')
	    ->select('bu.last_name as last_name', 'bu.first_name as first_name', 'bu.email as email','bu.role_id')
	    ->join('rea_plugin_etapa_user_roles as eur', 'eur.roleId', '=', 'bu.role_id')
	    ->join('rea_plugin_etapa as e', 'e.id', '=', 'eur.etapaId')
	    ->where('e.code', $code)
	    ->get();

    	return $users;
    }
}
