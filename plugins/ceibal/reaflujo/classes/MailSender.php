<?php namespace Ceibal\ReaFlujo\Classes;

use Backend;
use Mail;

class MailSender {

        public static function send($user, $data, $template){

            $data['link'] = Backend::url('ceibal/rea/recursos/update/'.$data['id']);

            Mail::send('ceibal.reaflujo::mail.'.$template, $data, function ($message) use ($user, $data) {
                $message->to("{$user->email}", "{$user->first_name}  {$user->last_name}")
                        ->from("noreply@ceibal.edu.uy")
                        ->subject("Proceso de publicación de recursos {$data['recurso']}.");
            });

        }

        public static function sendMany($users, $data, $template){
            foreach ($users as $user) {
                $data['name'] = "{$user->first_name} {$user->last_name}";
                MailSender::send($user, $data, $template);
            }
        }
    }

?>
